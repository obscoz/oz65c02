// Copyright (C) 2022-2023 Jean-Noël Vittaut aka Obscoz

// This file is part of oz65c02.

// oz65c02 is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// oz65c02 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with oz65c02.  If not, see <http://www.gnu.org/licenses/>

#ifndef types_hpp
#define types_hpp

#include <bit>
#include <cstdint>

namespace oz65c02 {

using u8 = uint8_t;
using u16 = uint16_t;
using u32 = uint32_t;
using u64 = uint64_t;

enum class CpuType : u8 { N6502, WDC65C02, ORIGINAL65C02 };

template <std::endian>
struct bytex2 {};

template <> // little endian
struct bytex2<std::endian::little> {
  u8 l, h;
};

template <> // big endian
struct bytex2<std::endian::big> {
  u8 h, l;
};

struct u8pair : bytex2<std::endian::native> {

  constexpr inline u8pair(u16 w) {
    l = u8(w);
    h = u8(w >> 8);
  }

  constexpr inline u8pair(u8 h_, u8 l_) {
    l = l_;
    h = h_;
  }

  constexpr inline operator u16() const {
    return u16((h << 8) | l);
  }

};

template <std::endian>
struct bytex8 {};

template <> // little endian
struct bytex8<std::endian::little> {
  // C : bit 0
  u8 c, z, i, d, br, on, v, n;
};

template <> // big endian
struct bytex8<std::endian::big> {
  u8 n, v, on, br, d, i, z, c;
};

struct u8x8 : bytex8<std::endian::native> {
  // clang ok with -O2
  // gcc needs -03 to optimize this code
  // msvc is a lost cause
  constexpr inline u8x8(u8 p) {
    u64 x = (p * 0x0101010101010101ULL) & 0x8040201008040201ULL;
    x = (x * 0xff >> 7) & 0x0101010101010101ULL;
    n = u8(x >> 56); v = u8(x >> 48); on = u8(x >> 40); br = u8(x >> 32);
    d = u8(x >> 24); i = u8(x >> 16); z = u8(x >> 8); c = u8(x >> 0);
  }

  constexpr explicit inline operator u64() const {
    return (u64(n) << 56) | (u64(v) << 48) | (u64(on) << 40) | (u64(br) << 32)
    | (u64(d) << 24) | (u64(i) << 16) | (u64(z) << 8) | (u64(c) << 0);
  }

  constexpr inline operator u8() const {
    u64 y = (u64(*this) * 0xff) & 0x8040201008040201ULL;
    return (y * 0x0101010101010101ULL) >> 56;
  }

};

class CpuTrivialData {
public:
  u8 d = 0, rw = 1;
  u8pair ad = 0x0000;
  u8 a = 0xaa, x = 0x00, y = 0x00, q = 0x00;
  u8x8 p = 0b00110010;
  u8pair pc = 0x00ff, sp = 0x0100;

  //protected:
  u8pair dp = 0x00ff, cgl = 0xfffe;
  u8 ir = 0x00, n_irq_pnd = 1, n_int_pnd = 1, n_int_inp = 1;
  u8 ic = 0, t = 0;
  u8 insert_fetch = 0x00, unused = 0xaa;
}; // 32 bytes

} // namespace oz65c02




#endif /* types_hpp */
