CC=g++
CFLAGS=-O3 -W -Wall -std=c++20

default: test_rom_65c02 test_rom_6502

test_rom_65c02: test_rom.cpp types.hpp microcode.hpp process_microcode.hpp cpu.hpp
	$(CC) $(CFLAGS) -D CPUTYPE=1 -o test_rom_65c02 test_rom.cpp

test_rom_6502: test_rom.cpp types.hpp microcode.hpp process_microcode.hpp cpu.hpp
	$(CC) $(CFLAGS) -D CPUTYPE=0 -o test_rom_6502 test_rom.cpp

test_65c02: test_rom_65c02 tests/65c02_decimal_test.bin tests/65c02_extended_opcodes_test.bin
	./test_rom_65c02 tests/65c02_extended_opcodes_test.bin 0x0000 0x24f3 0x0400
	./test_rom_65c02 tests/65c02_decimal_test.bin 0x0000 0x044d 0x0400
	./test_rom_65c02 tests/6502_test.bin 0xc000 0xef2b
	./test_rom_65c02 tests/65C02_test.bin 0xc000 0xe0b3
	./test_rom_65c02 tests/65C02_interrupt_test.bin 0x000a 0x071b 0x0400

test_6502: test_rom_6502 tests/6502_decimal_test.bin tests/6502_functional_test.bin
	./test_rom_6502 tests/6502_functional_test.bin 0x0000 0x346b 0x0400
	./test_rom_6502 tests/6502_decimal_test.bin 0x0000 0x044d 0x0400
	./test_rom_6502 tests/6502_test.bin 0xc000 0xef2b
	./test_rom_6502 tests/6502_ufo_test.bin 0xc000 0xce3c
	./test_rom_6502 tests/6502_interrupt_test.bin 0x000a 0x06f7 0x0400

test: test_65c02 test_6502

extra/cpp_generator: extra/cpp_generator.cpp types.hpp microcode.hpp process_microcode.hpp
	$(CC) $(CFLAGS) -o extra/cpp_generator extra/cpp_generator.cpp

extra/rust_generator: extra/rust_generator.cpp types.hpp microcode.hpp process_microcode.hpp
	$(CC) $(CFLAGS) -o extra/rust_generator extra/rust_generator.cpp

generate_cpp: extra/cpp_generator
	./extra/cpp_generator

generate_rust: extra/rust_generator
	./extra/rust_generator


tests/65c02_decimal_test.bin: tests/65c02_decimal_test.ca65
	ca65 -l tests/65c02_decimal_test.lst tests/65c02_decimal_test.ca65
	ld65 tests/65c02_decimal_test.o -o tests/65c02_decimal_test.bin -m tests/65c02_decimal_test.map -C ca65.cfg

tests/65c02_extended_opcodes_test.bin: tests/65c02_extended_opcodes_test.ca65
	ca65 -l tests/65c02_extended_opcodes_test.lst tests/65c02_extended_opcodes_test.ca65
	ld65 tests/65c02_extended_opcodes_test.o -o tests/65c02_extended_opcodes_test.bin -m tests/65c02_extended_opcodes_test.map -C ca65.cfg

tests/6502_functional_test.bin: tests/6502_functional_test.ca65
	ca65 -l tests/6502_functional_test.lst tests/6502_functional_test.ca65
	ld65 tests/6502_functional_test.o -o tests/6502_functional_test.bin -m tests/6502_functional_test.map -C ca65.cfg

tests/6502_decimal_test.bin: tests/6502_decimal_test.ca65
	ca65 -l tests/6502_decimal_test.lst tests/6502_decimal_test.ca65
	ld65 tests/6502_decimal_test.o -o tests/6502_decimal_test.bin -m tests/6502_decimal_test.map -C ca65.cfg


clean:
	rm -f *.o
	rm -f test_rom_65c02
	rm -f test_rom_6502
	rm -f extra/cpp_generator
	rm -f extra/rust_generator

