// Copyright (C) 2022-2023 Jean-Noël Vittaut aka Obscoz

// This file is part of oz65c02.

// oz65c02 is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// oz65c02 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with oz65c02.  If not, see <http://www.gnu.org/licenses/>

#ifndef cpu_hpp
#define cpu_hpp

#include "types.hpp"
#include "process_microcode.hpp"
#include "microcode.hpp"

namespace oz65c02 {

template <CpuType CPU>
class Cpu : public CpuTrivialData {

public:

  constexpr inline void res_pull_down() noexcept {
    // assert res is up
    cgl.l = 0xfc;
    n_int_pnd = 0;
    n_int_inp = 0;
    q = 0;
  }

//  constexpr inline void res_pull_up() noexcept {
//    // res_pull_up() is not implemented. It is the responsibility of the user of this lib to stop calling cycle() to simulate a halt.;
//  }

  constexpr inline void nmi_pull_down() noexcept {
    // assert nmi is up
    if (cgl.l != 0xfc) {
      cgl.l = 0xfa;
      n_int_pnd = 0;
    }
  }

//  constexpr inline void nmi_pull_up() noexcept {
//    // nmi_pull_up() is not implemented. It is asserted that nmi is up when nmi_pull_down() is called.
//  }

  constexpr inline void irq_pull_down() noexcept {
    n_irq_pnd = 0;
    n_int_pnd &= p.i; // = 0 if already or P.I == 0
  }

  constexpr inline void irq_pull_up() noexcept {
    n_irq_pnd = 1;
    n_int_pnd = (cgl.l == 0xfe); // = 0 if RES or NMI
  }

  constexpr inline bool sync() noexcept {
    return q == 0;
  }

  constexpr inline void cycle() noexcept {
    ir = q == 0 ? d * n_int_inp : ir;
    u8 microinstr = micro_code[8 * ir + q];
    call_table[microinstr](this);
  }
  
  constexpr inline void cycle_at(u16 opcode_addr, u8 opcode) noexcept {
    q = 0;
    d = opcode;
    pc = opcode_addr + 1;
    cycle();
  }

public:
  void(*next_mi)(Cpu*) noexcept = call_table[0];

  template <u8 WR_MX>
  u8& to_register() {
    return *std::array<u8*, 16>{{
      &dp.l, &dp.h, &sp.l, &x,
      &y, &t, &a /* AXS */, &t /* stores p in t temporarily */,
      &pc.l, &pc.h, &a, &d /* None */,
      &d /* ML */, &d /* WAI */, &d /* DPH + 1 */, &d /* MEM */
    }}[WR_MX];
  }

  template <u64 MICRO_INSTR>
  constexpr inline
  void cycle_code() noexcept {
    constexpr u8 D_TO_REGISTER = u8(MICRO_INSTR >> 48);
    constexpr u16 EXTRA = u16(MICRO_INSTR >> 32);
    constexpr u8 WR_MX = (MICRO_INSTR >> 28) & 0xf;
    constexpr u8 R_MX = (MICRO_INSTR >> 24) & 0xf;
    [[maybe_unused]] constexpr u8 DPH_LD = (MICRO_INSTR >> 23) & 0x1;
    constexpr u8 AD_MX = (MICRO_INSTR >> 20) & 0x7;
    constexpr u8 IR_LD = (MICRO_INSTR >> 19) & 0x1;
    constexpr u8 INC_MX = (MICRO_INSTR >> 16) & 0x7;
    constexpr u8 LUOP = (MICRO_INSTR >> 12) & 0xf;
    constexpr u8 ALU_EN = (MICRO_INSTR >> 11) & 0x1;
    constexpr u8 SF_MX = (MICRO_INSTR >> 8) & 0x7;
    constexpr u8 A_MX = (MICRO_INSTR >> 6) & 0x3;
    constexpr u8 C_MX = (MICRO_INSTR >> 3) & 0x7;
    constexpr u8 NX_MX = (MICRO_INSTR >> 0) & 0x7;
    constexpr u8 MEM_W = (CPU == CpuType::N6502) ? WR_MX > 0xd : WR_MX == 0xf;
    constexpr bool END_D = (NX_MX == 0x5);

    // Set rw pin: 1 = read, 0 = write
    if constexpr (RES_READ_ONLY & EXTRA) {
      rw = cgl.l == 0xfc;
    } else if constexpr (RMW_WRITE_6502 & EXTRA) {
      rw = 0;
    } else {
      rw = !MEM_W;
    }

    if constexpr (D_TO_REGISTER > 0) {
      if constexpr (D_TO_REGISTER == ReadInto::P) {
        p = d | 0b00110000;
        n_int_pnd &= n_irq_pnd | p.i; // 0 if already or n_irq_pnd down, I clear
      } else {
        *std::array<u8*, 8>{
          { nullptr, &pc.l, &pc.h, &dp.l, &dp.h, &x, &t, nullptr }
        }[D_TO_REGISTER] = d;
      }
    }

    // Branch test
    constexpr u16 INFO = EXTRA >> 8;
    if constexpr (BRANCH_OP & EXTRA) {
      constexpr u8 SELECT_FLAG = (INFO >> 1);
      constexpr u8 SET = INFO & 0b1;
      u8 flag = std::array<u8, 4>{{ p.n, p.v, p.c, p.z }}[SELECT_FLAG];
      insert_fetch = (flag != SET);
      q += 2 * insert_fetch;
    }

    // BBR/BBS 65c02 branch tests
    if constexpr (BBR_BBS & EXTRA) {
      constexpr u8 MASK = u8(1) << INFO;
      insert_fetch = (t & MASK) == 0;
      q += 2 * insert_fetch;
    }

    if constexpr (MICRO_INSTR == 0x0400001032c818) {
      // WDC65C02 abs,X/Y FFA instead of PBA if write without page crossing
      ad = ic ? ad : dp;
    } else {
      ad = std::array<u16, 8>{{
        pc, dp, u8pair{dp.h, t}, ad, u8pair{0x00, dp.l}, sp,
        u8pair{0xff, dp.l}, cgl,
      }}[AD_MX];
    }

    // Put data on the r bus
    constexpr u8 BCG = EXTRA >> 8;
    u8 const r = std::array<u8, 16>{{
      a, dp.h, sp.l, x, y, t, 0 /* SPI */, p,
      pc.l, pc.h, dp.l, 0, 0 /* None */, 0 /* STP */, u8(a & x), BCG
    }}[R_MX];

    // ALU enabled
    if constexpr (ALU_EN) {
      // ALU
      u8 rx = std::array<int, 4>{{0, r, d, r | d}}[A_MX];
      u8 ry = std::array<int, 16>{{
        0, ~r & ~d, r & ~d, ~d, ~r & d, ~r, r ^ d, ~r | ~d,
        r & d, ~(r ^ d), r, r | ~d, d, ~r | d, r | d, 0xff
      }}[LUOP];
      u16 sum = u16(std::array<int, 8>{{
        rx + ry, rx + ry + 1, rx + ry + p.c, rx + ry + ic,
        (rx + ry) >> 1 | ((rx + ry) & 1) << 8,
        (rx + ry) >> 1 | p.c << 7 | ((rx + ry) & 1) << 8,
        rx + ry /* BIT */, (rx + t * 0xff + ic) /* ADSIC */
      }}[C_MX]);

      // Save signextend in T
      if constexpr (NX_MX == 0x1) { // EXIT.CC
        t = ry >> 7;
      }

      // Store internal carry
      if constexpr (NX_MX == 0x2 || NX_MX == 0x1
                    || MICRO_INSTR == 0x0300000300c840
                    || MICRO_INSTR == 0x0300000400c840
                    || MICRO_INSTR == 0x0000008302c840
                    || MICRO_INSTR == 0x0000000442c840
                    || MICRO_INSTR == 0x0000005442c840) {
        ic = sum >> 8;
      }

      // Set flags
      u8 previous_carry = p.c;
      if constexpr (C_MX != 6) { // Flags from the ALU
        u8 tmp_z = u8(sum) == 0;
        u8 tmp_n = u8(sum) >> 7;
        u8 tmp_v = ((rx ^ u8(sum)) & (ry ^ u8(sum)) & 0x80) != 0;
        switch (SF_MX) {
          case 0x0: break; // None
          case 0x6: p.v = tmp_v; [[fallthrough]];
          case 0x5: p.c = sum >> 8; [[fallthrough]];
          case 0x4: p.n = tmp_n; [[fallthrough]];
          case 0x3: p.z = tmp_z; break;
          case 0x7: p.n = tmp_n; p.z = tmp_z; p.v = tmp_v; break;
          default: break;
        }
      } else { // BIT instruction special flags
        p.z = (r & d) == 0;
        if constexpr (SF_MX == 0x7) {
          p.n = (d & 0x80) != 0;
          p.v = (d & 0x40) != 0;
        }
      }

      // BCD adjustment
      if constexpr (END_D) {
        constexpr bool ADD = (LUOP >> 3);
        if (p.d) { // ADC or SBC decimal adjustment if decimal flag set
          u8 low_nibble = (rx & 0xf) + (ry & 0xf) + previous_carry;
          if constexpr (ADD) {
            if (low_nibble >= 0xa) {
              low_nibble = ((low_nibble + 0x6) & 0xf) + 0x10;
            }
            sum = (rx & 0xf0) + (ry & 0xf0) + low_nibble;
            if constexpr (CPU == CpuType::N6502) {
              p.n = (sum & 0x80) == 0x80;
            }
            p.v = ((rx ^ u8(sum)) & (ry ^ u8(sum)) & 0x80) != 0;
            if (sum >= 0xa0) {
              sum += 0x60;
            }
            p.c = (sum >= 0x100);
          } else {
            if constexpr (CPU == CpuType::N6502) {
              if (!(low_nibble & 0x10)) {
                low_nibble = (low_nibble + (~0x6 + 1)) & 0xf;
              }
              sum = (rx & 0xf0) + (ry & 0xf0) + low_nibble;
              if (!(sum & 0x100)) {
                sum += (~0x60 + 1) & 0xff;
              }
            } else {
              if (!(sum & 0x100)) {
                sum += ~0x60 + 1;
              }
              if (!(low_nibble & 0x10)) {
                sum += ~0x06 + 1;
              }
            }
          }
          if constexpr (CPU != CpuType::N6502) {
            p.z = (sum & 0xff) == 0;
            p.n = (sum & 0x80) == 0x80;
          }
        }
      }


      // Write the ALU result to a register
      if constexpr (WR_MX == 0x6) { // AXS 6502 undocumented opcode
        a = x = sp.l = u8(sum);
      } else if constexpr (WR_MX != 0xb) { // Except None
        to_register<WR_MX>() = u8(sum);
      }

    }

    // Set flags for set and clear instructions
    switch (SF_MX) {
      case 0x1: { // OPCODE
        constexpr u8 SELECT_FLAG = (INFO >> 1);
        constexpr u8 SET = (SELECT_FLAG == 2) ? 0 : (INFO & 0b1);
        u8& flag
        = *std::array<u8*, 4>{{ &p.c, &p.i, &p.v, &p.d }}[SELECT_FLAG];
        flag = SET;
        if constexpr (SELECT_FLAG == 0x1) { // P.I
          if constexpr (SET) { // mask IRQ
            n_int_pnd = (cgl.l == 0xfe); // = 0 if RES or NMI
          } else { // IRQ allowed
            n_int_pnd &= n_irq_pnd; // = 0 if already or n_irq_pnd down
          }
        }
        break;
      }
      case 0x2: {
        p.i = 1;
        if constexpr (CPU != CpuType::N6502) {
          p.d = 0;
        }
        break;
      }
      default: break;
    }

    // Data to present on the data bus on a write cycle
    if constexpr (MEM_W) {
      static_assert (!ALU_EN);
      d = r;
      if constexpr (WR_MX == 0xe) { // NMOS undocumented opcode
        d = d & (dp.h + 1);
      }
      if constexpr (BRK_PHP & EXTRA) {
        d &= 0b11101111 | (n_int_inp * 0b00010000);
      }
    }

    // Next micro-instruction to fetch (with interruptions)
    constexpr bool BCD_EXTRA_CYCLE = END_D && (CPU != CpuType::N6502);
    switch (NX_MX) {
      case 0x0: { // NEXT
        if (INC_Q_2 & EXTRA) {
          q += 2;
        } else {
          q += 1;
        }
        break;
      }
      case 0x1: { // EXIT.CC (page boundary crossed)
        insert_fetch = !(ic ^ t);
        q += 1 + insert_fetch;
        break;
      }
      case 0x2: { // INCDPH.C (page boundary crossed)
        q += 1 + 2 * !ic;
        break;
      }
      case 0x3: ++q; break; // EXIT.BTF
      case 0x4: q = 0; break; // END
      case 0x5: { // END.D
        if constexpr (BCD_EXTRA_CYCLE) {
          q = (q + 1) * p.d;
        } else {
          q = 0;
        }
        break;
      }
      case 0x6: { // END.INT
        q = 0;
        cgl.l = 0xfe;
        n_int_pnd = n_irq_pnd | p.i;
        break;
      }
      case 0x7: { // END.ARNC
        q = 0;
        p.c = (a >> 7);
        break;
      }
      default: break;
    }

    // Store the byte that will be put on the data bus in the next cycle
    if constexpr (IR_LD) { // Fetch opcode or interrupt
      if constexpr (BCD_EXTRA_CYCLE) { // Except for 65c02 in decimal mode
        if (q == 0) {
          n_int_inp = n_int_pnd;
        }
      } else {
        n_int_inp = n_int_pnd | insert_fetch;
      }
      insert_fetch = 0;
    }

    // Incrementer
    switch (INC_MX) {
      case 0x0: {
        if constexpr (IR_LD || (NO_INCR & EXTRA)) {
          pc = ad + n_int_inp;
        } else {
          pc = ad + 1;
        }
        break;
      }
      case 0x1: pc = ad - 1; break;
      case 0x2: case 0x3: break;
      case 0x4: dp.l = ad + 1; break;
      case 0x5: dp.l = ad - 1; break;
      case 0x6: sp.l = ad + 1; break;
      case 0x7: sp.l = ad - 1; break;
      default: break;
    }

  }

  template <u64 MICRO_INSTR>
  static inline
  void microcode_caller(Cpu* self) noexcept {
    self->template cycle_code<MICRO_INSTR>();
  }

  template <u8 B, u8 E>
  static consteval
  auto make_table() {
    using FunType = void(*)(Cpu*) noexcept;
    if constexpr (E - B == 1) {
      return std::array<FunType, 1>{
        {&microcode_caller<instructions[B]>}
      };
    } else {
      std::array<FunType, E - B> res { {} };
      auto half1 = make_table<B, (B + E) / 2>();
      auto half2 = make_table<(B + E) / 2, E>();
      u16 i = 0;
      for (auto x : half1) {
        res[i] = x;
        ++i;
      }
      for (auto x : half2) {
        res[i] = x;
        ++i;
      }
      return res;
    }
  }

  static constexpr auto patched1 = patch<CPU>(microcode<CPU>());
  static constexpr auto patched = patch2<CPU>(patched1);

  static constexpr auto mi_size = micro_instructions_size(patched);

  static constexpr auto instructions = micro_instructions<mi_size>(patched);

  static constexpr auto micro_code = process_microcode(patched, instructions);

  static constexpr auto call_table = make_table<0, u8(instructions.size())>();

};

using Wdc65c02 = Cpu<CpuType::WDC65C02>;
using N6502 = Cpu<CpuType::N6502>;

} // namespace oz65c02

#endif /* cpu_hpp */
