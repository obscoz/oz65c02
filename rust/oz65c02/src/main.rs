use std::fs::read;
use std::env;
use std::time::Instant;

//mod wdc65c02;
mod n6502;

fn load_ram(filename: &str, len: u16) -> Vec<u8> {
    let mut ram: Vec<u8> = vec![0; len as usize];
    ram.append(& mut read(filename).expect("File not found"));
    ram
}

fn parse_u16(s: &String) -> u16 {
    let no_prefix = s.strip_prefix("0x").expect("No prefix 0x");
    return u16::from_str_radix(no_prefix, 16).expect("Not a valide hex");
}

pub fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() != 4 && args.len() != 5 {
        println!("Usage: {} filename base_addr stop_addr [start_addr]\n", args[0]);
        return;
    }

    let filename = &args[1];
    let base_addr: u16 = parse_u16(&args[2]);
    println!("File: {}\nBase address: {:04x}", filename, base_addr);
    let stop_addr: u16 = parse_u16(&args[3]);
    println!("Stop address: {:04x}", stop_addr);

    let mut ram = load_ram(filename, base_addr);

    if args.len() == 5 {
        let start_addr: u16 = parse_u16(&args[4]);
        println!("Start address: {:04x}", start_addr);
        ram[0xbffc] = 0x00; // for interrupts test
        //let start: wdc65c02::U8Pair = start_addr.into();
        let start: n6502::U8Pair = start_addr.into();
        ram[0xfffc] = start.l;
        ram[0xfffd] = start.h;
    }


    //let mut cpu = wdc65c02::Cpu::new();
    let mut cpu = n6502::Cpu::new();
    cpu.res_pull_down();

    let now = Instant::now();

    println!("Test case:");
    let mut cycles: u64 = 1;
    while stop_addr != cpu.pc.into() {
        cpu.cycle();

        let ad: u16 = cpu.ad.into();
        let sp: u16 = cpu.sp.into();
        let dp: u16 = cpu.dp.into();
        let pc: u16 = cpu.pc.into();
        let cgl: u16 = cpu.cgl.into();
        let ir: u16 = cpu.ir.into();
        let p: u8 = cpu.p.into();
//        if cycles > 84030549 && cycles < 84031000 {
//            println!("IR={:02x} tick={} A={:02x} X={:02x} Y={:02x} NV1BDIZC={:02x} SP={:04x} DP={:04x} PC={:04x} CGL={:04x} Q={}",
//                     ir, cycles, cpu.a, cpu.x, cpu.y, p, sp, dp, pc, cgl, cpu.q);
//        println!("{} {:04x} {:02x}", ["W", "R"][cpu.rw as usize], ad, [cpu.d, ram[ad as usize]][cpu.rw as usize]);
//            println!("");
//        }
        if cpu.rw != 0 { // read
            cpu.d = ram[ad as usize];
        } else { // write
            if ad == 0xa000 {
                print!(" {}", cpu.d);
            }

            // feedback register for interrupt test
            if ad == 0xbffc {
                let diff = cpu.d ^ ram[ad as usize];
                if diff & 0x01 != 0 {
                    if cpu.d & 0x01 != 0 {
                        cpu.irq_pull_down();
                    } else {
                        cpu.irq_pull_up();
                    }
                }
                if diff & 0x02 != 0 && cpu.d & 0x02 != 0 {
                    cpu.nmi_pull_down();
                }
            }

            ram[ad as usize] = cpu.d;
        }
        cycles = cycles + 1;
    }

    let duration = now.elapsed().as_nanos() as f64;
    let frequency = 1000.0 / (duration / cycles as f64);
    println!("");
    println!("Cycles: {}", cycles);
    println!("Time: {} ns", duration);
    println!("Frequency: {} MHz", frequency);
    println!("Test OK\n");
}
