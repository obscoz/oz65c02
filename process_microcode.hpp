// Copyright (C) 2022-2023 Jean-Noël Vittaut aka Obscoz

// This file is part of oz65c02.

// oz65c02 is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// oz65c02 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with oz65c02.  If not, see <http://www.gnu.org/licenses/>


#ifndef process_microcode_h
#define process_microcode_h

#include <algorithm>
#include <array>
#include <tuple>

namespace oz65c02 {

constexpr u32 BRANCH_OP = u64(1) << 0;
constexpr u32 BBR_BBS = u64(1) << 1;
constexpr u32 BRK_PHP = u64(1) << 2;
constexpr u32 NO_INCR = u64(1) << 3;
constexpr u32 RES_READ_ONLY = u64(1) << 4;
constexpr u32 INC_Q_2 = u64(1) << 5;
constexpr u32 RMW_WRITE_6502 = u64(1) << 6;

template <CpuType CPU>
consteval auto
patch(std::array<u32, 8 * 256> const& microcode) {
  std::array<u64, 8 * 256> patched = {{}};
  for (u16 opcode = 0; opcode < 256; ++opcode) {
    patched[opcode * 8] = 0ull;
    if constexpr (CPU == CpuType::N6502) {
      if ((opcode & 0x0d) == 0x08) { // NMOS 1 byte instruction
        patched[opcode * 8] = 0x00020000ull; // disable incrementer
      }
    } else {
      if ((opcode & 0x0d) == 0x08 || (opcode & 0xef) == 0xcb) {
        // CMOS 1 byte instruction
        patched[opcode * 8] = 0x00020000ull; // disable incrementer
      }
    }
    u8 shift = 1;
    for (u8 mi = 0; mi < 8 - shift; ++mi) {
      u64 mc = microcode[opcode * 8 + mi];
      if constexpr (CPU == CpuType::WDC65C02) {
        if (mc == 0x5f028800ull || mc == 0x5f02d800ull) {
          // Patch RMB & SMB
          u64 extra = u64(~(1 << ((opcode >> 4) & 0x7)) & 0xff) << 40;
          mc |= extra;
        }
      }
      if constexpr (CPU == CpuType::N6502) {
        u64 extra = RMW_WRITE_6502;
        if (mc == 0x5b32cd20 || mc == 0x5b32cd28
            || mc == 0x5b32cd80 || mc == 0x5b32cd90
            || mc == 0x50320c88 || mc == 0x5032fc80) {
          mc |= (extra << 32);
        }
      }
      patched[opcode * 8 + mi + shift] = mc;
      u8 nx_mx = (mc >> 0) & 0x7;
      if (nx_mx == 0x2) { // INCDPH.C extra cycle
        shift += 1;
        patched[opcode * 8 + mi + shift]
        = CPU == CpuType::N6502 ? 0x11120848ull : 0x1032c818ull;
        shift += 1;
        u64 extra = INC_Q_2;
        patched[opcode * 8 + mi + shift]
        = microcode[opcode * 8 + mi + 1] | extra << 32;
      }
      if (mc == 0x11120858ull || mc == 0x11220858ull
          || mc == 0x11320858ull || mc == 0x11320848ull) {
        // DPH := DPH + 0 -> DPH := B + 0
        patched[opcode * 8 + mi + shift] = 0x1002c818ull | (mc & 0x00300000ull);
      }
      if (opcode != 0x80 && mc == 0x8802c841 && mi == 0) { // branch op
        u64 extra = BRANCH_OP | u32(opcode & 0b11100000ull) << 3;
        patched[opcode * 8] |= extra << 32;
      }
      if ((mc == 0xb0080904ull || mc == 0xb008f904ull) && mi == 0) {
        // clear/set flag
        u64 extra = u32(opcode & 0b11100000ull) << 3;
        patched[opcode * 8 + mi + shift] |= extra << 32;
      }
      if constexpr (CPU == CpuType::WDC65C02) {
        if (mc == 0xa008ce55ull || mc == 0xa0083e55ull) { // ADC/SBC
          shift += 1;
          patched[opcode * 8 + mi + shift] = 0xb03a0004ull;
        }
        if ((opcode & 0xf) == 0xf && mi == 1) { // bbr/bbs
          u64 extra = BBR_BBS | u32(opcode & 0b01110000ull) << 4;
          patched[opcode * 8 + mi + shift] |= 0x00020000ull;
          shift += 1;
          patched[opcode * 8 + mi + shift] = (extra << 32);
        }
        if ((opcode & 0x7) == 0x3) { // single-cycle NOP
          patched[opcode * 8] = 0x00080004ull;
          patched[opcode * 8 + 1] = 0x00000000ull;
        }
      }
    }
  }

  patched[0x00 * 8 + 3] = 0xf7570000ull | (u64(BRK_PHP) << 32); // Patch BRK php
  patched[0x00 * 8 + 0]
  = 0x00000000 | (u64(NO_INCR) << 32); // Patch BRK disable incr by default

  if constexpr (CPU == CpuType::WDC65C02) {
    patched[0x14 * 8 + 2] = 0x50424b30ull; // Patch TRB
    patched[0x14 * 8 + 4] = 0x00080004ull; // Patch TRB
    patched[0x1c * 8 + 3] = 0x50124b30ull; // Patch TRB
    patched[0x1c * 8 + 5] = 0x00080004ull; // Patch TRB

    patched[0x04 * 8 + 2] = 0x5042eb30ull; // Patch TSB
    patched[0x04 * 8 + 4] = 0x00080004ull; // Patch TSB
    patched[0x0c * 8 + 3] = 0x5012eb30ull; // Patch TSB
    patched[0x0c * 8 + 5] = 0x00080004ull; // Patch TSB
    
    // Add DPH.db for STZ abs,X
    patched[0x9e * 8 + 1] = 0x0380c840ull;
  }

  // Reset: flag to change to read only
  u64 extra = RES_READ_ONLY;
  patched[0x00 * 8 + 1] |= extra << 32;
  patched[0x00 * 8 + 2] |= extra << 32;
  patched[0x00 * 8 + 3] |= extra << 32;
  
  // Add DPH.db for STA abs,X
  patched[0x9d * 8 + 1] = 0x0380c840ull;
  // Add DPH.db for STA abs,Y
  patched[0x99 * 8 + 1] = 0x0480c840ull;

  return patched;
}

enum ReadInto: u8 { NONE, PCL, PCH, DPL, DPH, X, T, P };

template <CpuType CPU>
consteval auto
patch2(std::array<u64, 8 * 256> const& microcode) {
  [[maybe_unused]] bool assert;
  std::array<u64, 8 * 256> patched = {{}};
  for (u16 opcode = 0; opcode < 256; ++opcode) {
    patched[opcode * 8 + 0] = microcode[opcode * 8 + 0];
    for (u8 mi = 1; mi < 8; ++mi) {
      u64 mc = microcode[opcode * 8 + mi];
      u64& mc_before = patched[opcode * 8 + mi - 1];
      u8 DPH_LD = (mc_before >> 23) & 0x1;
      u8 ALU_EN = (mc_before >> 11) & 0x1;
      u8 WR_MX = (mc_before >> 28) & 0xf;
      ReadInto read_into = ReadInto::NONE;
      if (INC_Q_2 & mc_before >> 32) {
        read_into = ReadInto::DPH;
      } else if (DPH_LD) {
        read_into = ReadInto::DPH;
        mc_before &= ~(1ull << 23);
      } else if (!ALU_EN) {
        switch (WR_MX) {
          case 0x0: {
            bool DPH_LD = ((mc >> 23) & 0x1) != 0;
            u8 WR_MX = (mc >> 28) & 0xf;
            u8 AD_MX = (mc >> 20) & 0x7;
            if (DPH_LD || WR_MX == 0x1 || AD_MX == 0x4) {
              read_into = ReadInto::DPL;
            }
            break;
          }
          case 0x1: read_into = ReadInto::DPH; break;
          case 0x3: read_into = ReadInto::X; break;
          case 0x5: read_into = ReadInto::T; break;
          case 0x7: read_into = ReadInto::P; break;
          case 0x8: read_into = ReadInto::PCL; break;
          case 0x9: read_into = ReadInto::PCH; break;
          default:
            assert = 1 / WR_MX < 0xe;
            assert = 1 / WR_MX != 0x2;
            assert = 1 / WR_MX != 0x4;
            assert = 1 / WR_MX != 0x6;
            break;
        }
        if (WR_MX < 0xe) {
          // exclude DPH+1 for NMOS special opcode and write cycles
          mc_before &= 0xffffffff0fffffff;
        }
      }
      patched[opcode * 8 + mi] = mc | u64(read_into) << 48;
    }
  }

  // No fetch opcode for BRK
  patched[0 * 8 + 0] = microcode[0 * 8 + 0] | u64(ReadInto::NONE) << 48;
  
  // Load DPL for JMP
  patched[0x20 * 8 + 1] = microcode[0x20 * 8 + 1] | u64(ReadInto::DPL) << 48;


  return patched;
}


consteval auto micro_instructions_size(auto const& microcode) {
  auto micro_instructions = std::array<u64, 256> { {} };
  u16 size = 1;
  for (u16 opcode = 0; opcode < 256; ++opcode) {
    for (u8 mi = 0; mi < 8; ++mi) {
      auto mc = microcode[opcode * 8 + mi];
      auto found = std::ranges::find(micro_instructions, mc);
      if (found == micro_instructions.end()) {
        micro_instructions[size] = mc;
        ++size;
      }
    }
  }
  return size;
}

template <u8 Size>
consteval auto micro_instructions(auto const& microcode) {
  auto micro_instructions = std::array<u64, Size> { {} };
  u16 size = 1;
  for (u16 opcode = 0; opcode < 256; ++opcode) {
    for (u8 mi = 0; mi < 8; ++mi) {
      auto mc = microcode[opcode * 8 + mi];
      auto found = std::ranges::find(micro_instructions, mc);
      if (found == micro_instructions.end()) {
        micro_instructions[size] = mc;
        ++size;
      }
    }
  }
  return micro_instructions;
}

consteval auto process_microcode(auto const& microcode,
                                 auto const& micro_instructions) {
  auto microcode_ids = std::array<u8, 256 * 8> { {} };
  auto mibegin = micro_instructions.begin();
  for (u16 opcode = 0; opcode < 256; ++opcode) {
    for (u8 mi = 0; mi < 8; ++mi) {
      auto mc = microcode[opcode * 8 + mi];
      auto found = std::ranges::find(micro_instructions, mc);
      microcode_ids[opcode * 8 + mi] = u8(std::distance(mibegin, found));
    }
  }
  return microcode_ids;
}

} // namespace oz65c02

#endif /* process_microcode_h */
