// Copyright (C) 2022-2023 Jean-Noël Vittaut aka Obscoz

// This file is part of oz65c02.

// oz65c02 is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// oz65c02 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with oz65c02.  If not, see <http://www.gnu.org/licenses/>

#include <cstdio>
#include <cstdlib>
#include <chrono>

#include "cpu.hpp"
//#include "extra/Wdc65c02.hpp"
//#include "extra/N6502.hpp"

using oz65c02::u8;
using oz65c02::u16;
using oz65c02::u64;
using oz65c02::Wdc65c02;
using oz65c02::N6502;

#ifndef CPUTYPE
#define CPUTYPE 0
#endif

u8 ram[64 * 1024];
char const* cpu_type_name[] = { "NMOS 6502", "WDC 65c02", "Original 65c02" };

int main(int argc, char const** argv) {
  if (argc != 4 && argc != 5) {
      fprintf(stderr,
              "Usage: %s filename base_addr stop_addr [start_addr]\n", argv[0]);
      return EXIT_FAILURE;
  }

#if CPUTYPE == 0
  N6502 cpu;
#endif

#if CPUTYPE == 1
  Wdc65c02 cpu;
#endif

  char const* filename = argv[1];
  u16 base_addr = strtoul(argv[2], nullptr, 16);
  printf("CPU: %s\n", cpu_type_name[CPUTYPE]);
  printf("File: %s\nBase address: %04x\n", filename, base_addr);
  u16 stop_addr = strtoul(argv[3], nullptr, 16);
  printf("Stop address: %04x\n", stop_addr);

  auto romfile = fopen(filename, "r");
  if (romfile) {
    int c;
    while ((c = fgetc(romfile)) != EOF) {
      ram[base_addr++] = c;
    }
  }
  fclose(romfile);

  if (argc == 5) {
    u16 start_addr = strtoul(argv[4], nullptr, 16);
    printf("Start address: %04x\n", start_addr);
    ram[0xbffc] = 0x00; // for interrupts test
    oz65c02::u8pair start = start_addr;
    ram[0xfffc] = start.l;
    ram[0xfffd] = start.h;
  }

  auto begin = std::chrono::steady_clock::now();

  printf("Test case:\n");
  u64 cycles;
  cpu.res_pull_down();
  for (cycles = 1; cpu.pc != stop_addr /*&& cycles < 1500*/; ++cycles) {
//    if (cycles == 470) {
//      printf("Stop\n");
//    }
//    if (true or (cycles >= 32100 && cycles <= 32300)) {
//      printf("IR=%02x tick=%llu A=%02x X=%02x Y=%02x NV1BDIZC=%02x SP=%04x DP=%04x PC=%04x CGL=%04x Q=%1d\n",
//             u8(cpu.ir), cycles, u8(cpu.a), u8(cpu.x), u8(cpu.y), u8(cpu.p), u16(cpu.sp), u16(cpu.dp), u16(cpu.pc), u16(cpu.cgl), cpu.q);
//      printf("N_INT_INP=%1x N_INT_PND=%1x N_IRQ=%1x\n",
//             cpu.n_int_inp, cpu.n_int_pnd, cpu.n_irq_pnd);
//      printf("%c %04x %02x\n", cpu.rw ? 'R' : 'W', u16(cpu.ad), cpu.rw? ram[cpu.ad] : cpu.d);
//      printf("\n");
//    }


    if (cpu.rw) { // read
      cpu.d = ram[cpu.ad];
    } else { // write

      if (cpu.ad == 0xa000) {
        printf(" %d", cpu.d);
      }

      // feedback register for interrupt test
      if (cpu.ad == 0xbffc) [[unlikely]] {
        u8 diff = (cpu.d ^ ram[cpu.ad]);
        if (diff & 0x01) {
          if (cpu.d & 0x01) {
            cpu.irq_pull_down();
          } else {
            cpu.irq_pull_up();
          }
        }
        if (diff & 0x02 && cpu.d & 0x02) {
          cpu.nmi_pull_down();
        }
      }

      ram[cpu.ad] = cpu.d;
    }
    cpu.cycle();
  }

  auto end = std::chrono::steady_clock::now();
  auto duration
  = std::chrono::duration_cast<std::chrono::nanoseconds>(end - begin).count();
  auto frequency = 1000.0 / (double(duration) / cycles);
  printf("\nCycles: %llu\n", cycles);
  printf("Time: %llu ns\n", duration);
  printf("Frequency: %f MHz\n", frequency);
  printf("Zero page:\n   ");
  for (int i = 0; i < 0x10; ++i) {
    printf(" -%x", i);
  }
  for (int i = 0; i < 0x100; ++i) {
    if (i % 16 == 0) {
      printf("\n%x-:", i / 16);
    }
    printf(" %02x", ram[i]);
  }
  printf("\nTest ended\n\n");

  return EXIT_SUCCESS;
}
